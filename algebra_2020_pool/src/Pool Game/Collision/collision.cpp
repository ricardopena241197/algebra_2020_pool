#include "collision.h"
#include <iostream>
#include <vector>
#include "Pool Game/General/customMath.h"
#include "Pool Game/Objects/poolTable.h"



void BallsCollision(Ball* balls[], const int BALL_AMMOUNT, int i)
{

    float ballsDistance = 0.0f;
    float overlap = 0;
    std::vector < std::pair < Ball*, Ball* >> ballCollisions;

    for (int j = 0; j < BALL_AMMOUNT; j++)
    {
        if (j != i)
        {
            ballsDistance = distance2D(balls[i]->getPosition(), balls[j]->getPosition());
            if (ballsDistance < balls[i]->getRadius() + balls[j]->getRadius())
            {
                ballCollisions.push_back({ balls[i], balls[j] });

                overlap = 0.5f * (ballsDistance - balls[i]->getRadius() - balls[j]->getRadius());

                float newXPosition = balls[i]->getPosition().x - overlap * (balls[i]->getPosition().x - balls[j]->getPosition().x) / ballsDistance;
                float newYPosition = balls[i]->getPosition().y - overlap * (balls[i]->getPosition().y - balls[j]->getPosition().y) / ballsDistance;

                balls[i]->setPosition({ newXPosition, newYPosition });

                newXPosition = balls[j]->getPosition().x + overlap * (balls[i]->getPosition().x - balls[j]->getPosition().x) / ballsDistance;
                newYPosition = balls[j]->getPosition().y + overlap * (balls[i]->getPosition().y - balls[j]->getPosition().y) / ballsDistance;

                balls[j]->setPosition({ newXPosition, newYPosition });
            }
        }
    }
    for (auto ballCollided : ballCollisions)
    {
        Ball* ball1 = ballCollided.first;
        Ball* ball2 = ballCollided.second;

        ballsDistance = distance2D(ball1->getPosition(), ball2->getPosition());

        float normalX = (ball2->getPosition().x - ball1->getPosition().x) / ballsDistance;
        float normalY = (ball2->getPosition().y - ball1->getPosition().y) / ballsDistance;

        float tangentX = -normalY;
        float tangentY = normalX;

        float dotProductTangent1 = ball1->getVelocity().x * tangentX + ball1->getVelocity().y * tangentY;
        float dotProductTangent2 = ball2->getVelocity().x * tangentX + ball2->getVelocity().y * tangentY;

        float dotProductNormal1 = ball1->getVelocity().x * normalX + ball1->getVelocity().y * normalY;
        float dotProductNormal2 = ball2->getVelocity().x * normalX + ball2->getVelocity().y * normalY;

        float momentumConservation1 = (dotProductNormal1 / (BALL_MASS * 2)) + dotProductNormal2;
        float momentumConservation2 = (dotProductNormal2 / (BALL_MASS * 2)) + dotProductNormal1;

        ball1->setVelocity({ tangentX * dotProductTangent1 + normalX * momentumConservation1, tangentY * dotProductTangent1 + normalY * momentumConservation1 });
        ball2->setVelocity({ tangentX * dotProductTangent2 + normalX * momentumConservation2, tangentY * dotProductTangent2 + normalY * momentumConservation2 });
    }
}

void PoolTableCollision(Border* borders[], Ball* ball)
{
    if (ball->getActive())
    {
        if (ball->getPosition().y - ball->getRadius() <= borders[0]->getCollider().y + borders[0]->getCollider().height && ball->getPosition().x >= borders[0]->getCollider().x && ball->getPosition().x <= borders[0]->getCollider().x + borders[0]->getCollider().width)
        {
            Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
            ball->setVelocity(newDirection);
            ball->setPosition({ ball->getPosition().x, borders[0]->getCollider().y + borders[0]->getCollider().height + ball->getRadius() });
        }
        else if (ball->getPosition().y - ball->getRadius() <= borders[1]->getCollider().y + borders[1]->getCollider().height && ball->getPosition().x >= borders[1]->getCollider().x && ball->getPosition().x <= borders[1]->getCollider().x + borders[1]->getCollider().width)
        {
            Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
            ball->setVelocity(newDirection);
            ball->setPosition({ ball->getPosition().x, borders[1]->getCollider().y + borders[1]->getCollider().height + ball->getRadius() });
        }
        else if (ball->getPosition().x - ball->getRadius() <= borders[2]->getCollider().x + borders[2]->getCollider().width && ball->getPosition().y >= borders[2]->getCollider().y && ball->getPosition().y <= borders[2]->getCollider().y + borders[2]->getCollider().height)
        {
            Vector2 newDirection = { ball->getVelocity().x * -1, ball->getVelocity().y };
            ball->setVelocity(newDirection);
            ball->setPosition({ borders[2]->getCollider().x + borders[2]->getCollider().width + ball->getRadius(), ball->getPosition().y });
        }
        else if (ball->getPosition().y + ball->getRadius() >= borders[3]->getCollider().y && ball->getPosition().x >= borders[3]->getCollider().x && ball->getPosition().x <= borders[3]->getCollider().x + borders[3]->getCollider().width)
        {
            Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
            ball->setVelocity(newDirection);
            ball->setPosition({ ball->getPosition().x, borders[3]->getCollider().y - ball->getRadius() });
        }
        else if (ball->getPosition().y + ball->getRadius() >= borders[4]->getCollider().y && ball->getPosition().x >= borders[4]->getCollider().x && ball->getPosition().x <= borders[4]->getCollider().x + borders[4]->getCollider().width)
        {

            Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
            ball->setVelocity(newDirection);
            ball->setPosition({ ball->getPosition().x,  borders[4]->getCollider().y - ball->getRadius() });
        }
        else if (ball->getPosition().x + ball->getRadius() >= borders[5]->getCollider().x && ball->getPosition().y >= borders[5]->getCollider().y && ball->getPosition().y <= borders[5]->getCollider().y + borders[5]->getCollider().height)
        {
            Vector2 newDirection = { ball->getVelocity().x * -1, ball->getVelocity().y };
            ball->setVelocity(newDirection);
            ball->setPosition({ borders[5]->getCollider().x - ball->getRadius(), ball->getPosition().y });
        }

    }
   
    /*
    if (ball->getPosition().x + ball->getRadius() >= POOL_TABLE.x + POOL_TABLE.width - POOL_TABLE_FRAME_SIZE)
    {
        Vector2 newDirection = { ball->getVelocity().x * -1, ball->getVelocity().y };
        ball->setVelocity(newDirection);
        ball->setPosition({ POOL_TABLE.x + POOL_TABLE.width - POOL_TABLE_FRAME_SIZE - ball->getRadius(), ball->getPosition().y});
    }
    else if (ball->getPosition().x - ball->getRadius() <= POOL_TABLE.x + POOL_TABLE_FRAME_SIZE)
    {
        Vector2 newDirection = { ball->getVelocity().x * -1, ball->getVelocity().y };
        ball->setVelocity(newDirection);
        ball->setPosition({ POOL_TABLE.x + POOL_TABLE_FRAME_SIZE + ball->getRadius(), ball->getPosition().y });
    }
    else if (ball->getPosition().y + ball->getRadius() >= POOL_TABLE.y + POOL_TABLE.height - POOL_TABLE_FRAME_SIZE)
    {
        Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
        ball->setVelocity(newDirection);
        ball->setPosition({ball->getPosition().x, POOL_TABLE.y + POOL_TABLE.height - POOL_TABLE_FRAME_SIZE - ball->getRadius()});
    }
    else if (ball->getPosition().y - ball->getRadius() <= POOL_TABLE.y + POOL_TABLE_FRAME_SIZE)
    {
        Vector2 newDirection = { ball->getVelocity().x, ball->getVelocity().y * -1 };
        ball->setVelocity(newDirection);
        ball->setPosition({ ball->getPosition().x, POOL_TABLE.y + POOL_TABLE_FRAME_SIZE + ball->getRadius() });
    }
    */
}

void PoolTableHolesCollision(Hole* holes[], Ball* ball)
{  
    if (ball->getActive())
    {
        for (short i = 0; i < POOL_TABLE_HOLES; i++)
        {
            if (distance2D(ball->getPosition(), holes[i]->getPosition()) < holes[i]->getRadius())
            {               
                ball->setVelocity({ 0,0 });
                ball->setActive(false);
            }
        }
    }
    
}