#ifndef COLISION_H
#define COLISION_H

#include "raylib.h"
#include "Pool Game/Objects/ball.h"
#include "Pool Game/Objects/hole.h"
#include "Pool Game/Objects/border.h"

void BallsCollision(Ball* balls[], const int BALL_AMMOUNT, int i);

void PoolTableCollision(Border* borders[], Ball* ball);

void PoolTableHolesCollision(Hole* holes[], Ball* ball);
#endif
