#ifndef POOL_GAME_H
#define POOL_GAME_H

#include "Objects/ball.h"
#include "Objects/poolTable.h"

const int BALLS_AMMOUNT = 16;

class PoolGame
{
public:
	PoolGame();
	~PoolGame();
	void play();
private:
	PoolTable* poolTable;
	Ball* balls[BALLS_AMMOUNT];
	bool gameOver;
	bool win;

	void input();
	void resetGame();
	void update();
	void draw();
	bool gameOverCheck();
};

#endif
